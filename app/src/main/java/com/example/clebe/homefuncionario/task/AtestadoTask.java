package com.example.clebe.homefuncionario.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.clebe.homefuncionario.WebClient;
import com.example.clebe.homefuncionario.converter.AtestadoConverter;
import com.example.clebe.homefuncionario.dao.AtestadoDB;
import com.example.clebe.homefuncionario.model.Atestado;

import java.util.List;

/**
 * Created by clebe on 30/03/2018.
 */

public class AtestadoTask extends AsyncTask<Void, Void, String> {

    private Context context;
    private ProgressDialog progress;

    public AtestadoTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        progress = ProgressDialog.show(context,"Aguarde", "Enviando os dados...", true, true);
    }

    @Override
    protected String doInBackground(Void... objects) {
        AtestadoDB dao = new AtestadoDB(context);
        List<Atestado> lista = dao.getAtestados();
        dao.close();

        String json  = new AtestadoConverter().listToJson(lista);
        return new WebClient().postAtestadoList(json);
    }

    @Override
    protected void onPostExecute(String resposta) {
        progress.dismiss();
        Toast.makeText(context, resposta, Toast.LENGTH_LONG).show();
    }
}
