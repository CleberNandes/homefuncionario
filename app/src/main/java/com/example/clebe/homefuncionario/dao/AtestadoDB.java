package com.example.clebe.homefuncionario.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.example.clebe.homefuncionario.model.Atestado;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clebe on 11/03/2018.
 */

public class AtestadoDB extends SQLiteOpenHelper{

    public AtestadoDB(Context context) {
        super(context, "homeFunc", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table atestados (" +

            "id integer primary key ," +
            "crm_medico text not null," +
            "nome_medico text null," +
            "cid text," +
            "data text not null," +
            "duracao text not null," +
            "motivo text not null" +
            "caminhoFoto text"+
        ");";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql;
        switch(oldVersion){
            case 1:
                sql = "alter table atestados add column caminhoFoto text";
                db.execSQL(sql);
            case 2:
                sql = "alter table atestados add column hospital text";
                db.execSQL(sql);
        }

    }

    public void insere(Atestado atestado) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = getContentValuesAtestado(atestado);

        long id = db.insert("atestados", null, values);
        atestado.setId(id);
    }

    @NonNull
    private ContentValues getContentValuesAtestado(Atestado atestado) {
        ContentValues values = new ContentValues();

        values.put("hospital", atestado.getHospital());
        values.put("crm_medico", atestado.getCrm_medico());
        values.put("nome_medico", atestado.getNome_medico());
        values.put("cid", atestado.getCid());
        values.put("data", atestado.getData());
        values.put("duracao", atestado.getDuracao());
        values.put("motivo", atestado.getMotivo());
        values.put("caminhoFoto", atestado.getCaminhoFoto());
        return values;
    }


    public List<Atestado> getAtestados() {
        String sql = "select * from atestados";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        c.moveToNext();
        List<Atestado> atestados = new ArrayList<Atestado>();
        while(c.moveToNext()){
            Atestado atestado = new Atestado();

            atestado.setId(c.getLong(c.getColumnIndex("id")));
            atestado.setHospital(c.getString(c.getColumnIndex("hospital")));
            atestado.setCrm_medico(c.getString(c.getColumnIndex("crm_medico")));
            atestado.setNome_medico(c.getString(c.getColumnIndex("nome_medico")));
            atestado.setCid(c.getString(c.getColumnIndex("cid")));
            atestado.setData(c.getString(c.getColumnIndex("data")));
            atestado.setDuracao(c.getString(c.getColumnIndex("duracao")));
            atestado.setMotivo(c.getString(c.getColumnIndex("motivo")));
            atestado.setCaminhoFoto(c.getString(c.getColumnIndex("caminhoFoto")));

            atestados.add(atestado);
        }
        c.close();
        return atestados;
    }

    public void delete(Atestado atestado) {
        SQLiteDatabase db = getWritableDatabase();
        String[] params = {atestado.getId().toString()};
        db.delete("atestados", "id = ?",params);
    }

    public void update(Atestado atestado) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = getContentValuesAtestado(atestado);
        String[] params = {atestado.getId().toString()};
        db.update("atestados",dados, "id = ?", params);
    }

    public boolean ehAtestado(String telefone){
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("select * from atestados where cid = ?", new String[]{telefone});
        int result = c.getCount();
        c.close();
        return result > 0;
    }
}
