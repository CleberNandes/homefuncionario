package com.example.clebe.homefuncionario;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by clebe on 29/03/2018.
 */

public class WebClient {
    public String postAtestadoList(String json){
        URL url = null;
        String endereco = "http://192.168.0.2:9000/api/atestado";
        return post(json, endereco);
    }

    public String sendAtestado(String json) {
        URL url = null;
        String endereco = "http://192.168.0.2:9000/api/atestado";
        return post(json, endereco);
    }

    private String post(String json, String endereco) {
        URL url;
        try {
            url = new URL(endereco);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            PrintStream output = new PrintStream(connection.getOutputStream());
            output.println(json);

            connection.connect();
            return new Scanner(connection.getInputStream()).next();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Não foi possivel se conectar";
    }

}
