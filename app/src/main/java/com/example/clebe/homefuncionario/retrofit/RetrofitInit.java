package com.example.clebe.homefuncionario.retrofit;


import com.example.clebe.homefuncionario.services.AtestadoService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by clebe on 01/04/2018.
 */

public class RetrofitInit {
    private final Retrofit retrofit;

    public RetrofitInit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(interceptor);

        retrofit = new Retrofit.Builder().baseUrl("http://192.168.0.2:9000/api/")
                .addConverterFactory(JacksonConverterFactory.create())
                .client(client.build())
                .build();
    }

    public AtestadoService getAtestadoService() {

        return retrofit.create(AtestadoService.class);
    }
}
