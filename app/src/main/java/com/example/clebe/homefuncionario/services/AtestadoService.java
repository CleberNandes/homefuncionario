package com.example.clebe.homefuncionario.services;


import com.example.clebe.homefuncionario.model.Atestado;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by clebe on 01/04/2018.
 */

public interface AtestadoService {

    @POST("atestado")
    Call<Void> insere(@Body Atestado atestado);

    @POST("atestado")
    Call<Void> lista(@Body Atestado atestado);

}
