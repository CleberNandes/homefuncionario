package com.example.clebe.homefuncionario.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.example.clebe.homefuncionario.dao.AtestadoDB;

/**
 * Created by clebe on 27/03/2018.
 */

public class SmsReceiver extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        Object[] pdus = (Object[]) intent.getSerializableExtra("pdus");
        byte[] pdu = (byte[]) pdus[0];
        String formato = (String) intent.getSerializableExtra("fotmat");

        SmsMessage sms = SmsMessage.createFromPdu(pdu, formato);
        String telefone = sms.getDisplayOriginatingAddress();

        AtestadoDB dao = new AtestadoDB(context);
        if(dao.ehAtestado(telefone)){
            Toast.makeText(context, "Chegou um SMS", Toast.LENGTH_LONG).show();
            //MediaPlayer mp = MediaPlayer.create(context, R.raw.msg);
            //mp.start();
        }
        dao.close();
    }
}


