package com.example.clebe.homefuncionario;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.clebe.homefuncionario.model.Atestado;

/**
 * Created by clebe on 11/03/2018.
 */

public class AtestadoHelper {
    private final EditText crm;
    private final EditText nome;
    private final EditText cid;
    private final EditText data;
    private final EditText duracao;
    private final EditText motivo;
    private final ImageView campoFoto;
    private final EditText hospital;

    private Atestado atestado;


    public AtestadoHelper(AtestadoAct act){
        crm = act.findViewById(R.id.atest_crm_medico);
        nome = act.findViewById(R.id.atest_nome_medico);
        cid = act.findViewById(R.id.atest_cid);
        data = act.findViewById(R.id.atest_data);
        duracao = act.findViewById(R.id.atest_duracao);
        motivo = act.findViewById(R.id.atest_motivo);
        campoFoto = act.findViewById(R.id.atest_img);
        hospital = act.findViewById(R.id.atest_hospital);
        atestado = new Atestado();
    }

    public Atestado getAtestado() {

        atestado.setCrm_medico(crm.getText().toString());
        atestado.setNome_medico(nome.getText().toString());
        atestado.setCid(cid.getText().toString());
        atestado.setData(data.getText().toString());
        atestado.setDuracao(duracao.getText().toString());
        atestado.setMotivo(motivo.getText().toString());
        atestado.setCaminhoFoto((String) campoFoto.getTag());
        atestado.setHospital(hospital.getText().toString());
        return atestado;
    }

    public void fillForm(Atestado atestado) {
        crm.setText(atestado.getCrm_medico());
        nome.setText(atestado.getNome_medico());
        cid.setText(atestado.getCid());
        data.setText(atestado.getData());
        duracao.setText(atestado.getDuracao());
        motivo.setText(atestado.getMotivo());
        hospital.setText(atestado.getHospital());
        carregaImg(atestado.getCaminhoFoto());
        this.atestado = atestado;

    }

    public void carregaImg(String caminhoFoto) {

        if(caminhoFoto != null){
            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzida = Bitmap.createScaledBitmap(bitmap, 300,300, true);
            campoFoto.setImageBitmap(bitmapReduzida);
            campoFoto.setScaleType((ImageView.ScaleType.FIT_XY));
            campoFoto.setTag(caminhoFoto);
        }
    }
}
