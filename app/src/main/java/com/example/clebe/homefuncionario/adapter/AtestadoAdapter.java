package com.example.clebe.homefuncionario.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.clebe.homefuncionario.ListaAtestAct;
import com.example.clebe.homefuncionario.R;
import com.example.clebe.homefuncionario.model.Atestado;

import java.util.List;

/**
 * Created by clebe on 26/03/2018.
 */

public class AtestadoAdapter extends BaseAdapter {
    private final List<Atestado> lista;
    private final Context context;

    public AtestadoAdapter(Context context, List<Atestado> lista) {
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {

        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {

        return lista.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Atestado atestado = lista.get(position);
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = convertView;
        if(view == null){
            view = inflater.inflate(R.layout.item_atestado, parent, false);
        }
        ImageView campoFoto = view.findViewById(R.id.item_atest_foto);
        String caminhoFoto = atestado.getCaminhoFoto();

        if(caminhoFoto != null){
            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzida = Bitmap.createScaledBitmap(bitmap, 50,50, true);
            campoFoto.setImageBitmap(bitmapReduzida);
            campoFoto.setScaleType((ImageView.ScaleType.FIT_XY));
        }

        TextView campoCid = view.findViewById(R.id.item_atest_cid);
        campoCid.setText(atestado.getCid());
        TextView campoData = view.findViewById(R.id.item_atest_data);
        campoData.setText(atestado.getData());
        return view;
    }
}
