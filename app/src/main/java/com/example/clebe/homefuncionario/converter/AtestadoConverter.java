package com.example.clebe.homefuncionario.converter;

import android.widget.Toast;

import com.example.clebe.homefuncionario.model.Atestado;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

/**
 * Created by clebe on 29/03/2018.
 */

public class AtestadoConverter {
    public String listToJson(List<Atestado> lista) {
        JSONStringer json = new JSONStringer();
        try {
            json.object().key("atestado").array();
            for(Atestado atestado : lista){
                objectToJson(json, atestado);
            }
            json.endArray().endObject();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json.toString();
    }

    public String oneToJson(Atestado atestado) {
        JSONStringer json = new JSONStringer();
        try {
            objectToJson(json, atestado);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    private void objectToJson(JSONStringer json, Atestado atestado) throws JSONException {
        json.object()
            .key("hospital").value(atestado.getHospital())
            .key("crm_medico").value(atestado.getCrm_medico())
            .key("nome_medico").value(atestado.getNome_medico())
            .key("cid").value(atestado.getCid())
            .key("data").value(atestado.getData())
            .key("duracao").value(atestado.getDuracao())
            .key("motivo").value(atestado.getMotivo())
        .endObject();
    }
}
