package com.example.clebe.homefuncionario;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.clebe.homefuncionario.adapter.AtestadoAdapter;
import com.example.clebe.homefuncionario.dao.AtestadoDB;
import com.example.clebe.homefuncionario.model.Atestado;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

public class ListaAtestAct extends AppCompatActivity {

    private ListView atestados;
    private MaterialSearchView searchView;
    private List<Atestado> listaDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_lista_atest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {}

            @Override
            public void onSearchViewClosed() {
                carregaLista();
            }
        });
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {return false;}

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText != null && !newText.isEmpty()){
                    ArrayList<Atestado> itemFound = new ArrayList<Atestado>();
                    for (Atestado item: listaDao){
                        if(
                            item.getHospital().contains(newText) ||
                            item.getCrm_medico().contains(newText) ||
                            item.getNome_medico().contains(newText) ||
                            item.getCid().contains(newText) ||
                            item.getData().contains(newText) ||
                            item.getMotivo().contains(newText) ||
                            item.getDuracao().contains(newText)

                        )
                            itemFound.add(item);
                    }
                    AtestadoAdapter adapter = new AtestadoAdapter(ListaAtestAct.this, itemFound);
                    atestados.setAdapter(adapter);
                } else {
                    carregaLista();
                }
                return true;
            }

        });

        atestados = findViewById(R.id.lista_atestados);
        atestados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> lista, View item, int position, long id) {
                Atestado atestado = (Atestado) atestados.getItemAtPosition(position);
                Intent editAtestado = new Intent(ListaAtestAct.this, AtestadoAct.class);
                editAtestado.putExtra("atestado", atestado);
                startActivity(editAtestado);
                Toast.makeText(ListaAtestAct.this, "Modo de Edição!", Toast.LENGTH_SHORT).show();


            }
        });
        /*atestados.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> lista, View item, int position, long id) {
                Toast.makeText(ListaAtestAct.this, "Atestado longo clique", Toast.LENGTH_SHORT).show();
                return false;
            }
        });*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToFormAtest = new Intent(ListaAtestAct.this, AtestadoAct.class);
                startActivity(goToFormAtest);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        registerForContextMenu(atestados);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_list_atest, menu);

        MenuItem item = menu.findItem(R.id.list_atestado_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            //case R.id.dash_search:


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    private void carregaLista() {

        AtestadoDB dao = new AtestadoDB(this);
        listaDao = dao.getAtestados();
        dao.close();

        AtestadoAdapter adapter =
                new AtestadoAdapter(this, listaDao);
        atestados.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final Atestado atestado = (Atestado) atestados.getItemAtPosition(info.position);

        visitaSite(menu, atestado);

        enviaSms(menu, atestado);

        visualizaMapa(menu, atestado);

        MenuItem itemLigar = menu.add("Ligar");
        itemLigar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // verifica se tem permissão
                ActivityCompat.requestPermissions(ListaAtestAct.this,
                        new String[]{Manifest.permission.CALL_PHONE}, 123);
                if(ActivityCompat.checkSelfPermission(ListaAtestAct.this,Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED){

                } else {
                    Intent intentCall = new Intent(Intent.ACTION_CALL);
                    intentCall.setData(Uri.parse("tel:(11)970302337"));
                    startActivity(intentCall);
                }
                return false;
            }
        });




        deletaAtestado(menu, atestado);
    }

    private void deletaAtestado(ContextMenu menu, final Atestado atestado) {
        MenuItem deletar = menu.add("Deletar atestado médico");
        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

            AtestadoDB dao = new AtestadoDB(ListaAtestAct.this);
            dao.delete(atestado);
            dao.close();
            carregaLista();

            Toast.makeText(ListaAtestAct.this,
                    "Deletado com sucesso o "+atestado.getMotivo()+" "+atestado.getData(),
                    Toast.LENGTH_SHORT).show();

            return true;
            }
        });
    }

    private void visualizaMapa(ContextMenu menu, Atestado atestado) {
        MenuItem itemMapa = menu.add("Visualizar no mapa");
        Intent intentMapa = new Intent(Intent.ACTION_VIEW);
        intentMapa.setData(Uri.parse("geo:0,0?q=" + atestado.getCrm_medico()));
        itemMapa.setIntent(intentMapa);
    }

    private void enviaSms(ContextMenu menu, Atestado atestado) {
        MenuItem itemSms = menu.add("Enviar SMS");
        Intent intentSms = new Intent(Intent.ACTION_VIEW);
        intentSms.setData(Uri.parse("sms:(11)"+atestado.getCid()));
        itemSms.setIntent(intentSms);
    }

    private void visitaSite(ContextMenu menu, Atestado atestado) {
        MenuItem viewSite = menu.add("Visitar Site");
        Intent intentSite = new Intent(Intent.ACTION_VIEW);

        String site = atestado.getMotivo();
        if(!site.startsWith("http://")){
            site = "http://" + site;
        }

        intentSite.setData(Uri.parse(site));
        viewSite.setIntent(intentSite);
    }


}
