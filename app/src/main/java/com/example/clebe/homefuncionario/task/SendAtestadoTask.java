package com.example.clebe.homefuncionario.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.clebe.homefuncionario.AtestadoAct;
import com.example.clebe.homefuncionario.WebClient;
import com.example.clebe.homefuncionario.converter.AtestadoConverter;
import com.example.clebe.homefuncionario.model.Atestado;

/**
 * Created by clebe on 31/03/2018.
 */

public class SendAtestadoTask extends AsyncTask<Void, Void, String>{
    private Atestado atestado;
    private Context context;

    public SendAtestadoTask(Atestado atestado, Context context) {

        this.atestado = atestado;
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... context) {

        String json = new AtestadoConverter().oneToJson(atestado);
        return new WebClient().sendAtestado(json);

    }

    @Override
    protected void onPostExecute(String resposta) {

        Toast.makeText(context, resposta, Toast.LENGTH_LONG).show();
    }
}
