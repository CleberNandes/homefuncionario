# Trabalho Entrega Continuada #

Entrega do app da aula de Dispositivos m�veis com prof. Fernando

### 1� entrega do app ###

* Tela de Login
* Ap�s login redirecionar para a tela de dash

### 2� entrega ###

* Cores e padroes no colors.xml e styles.xml
* Aprimoramento da tela inicial
* Action bar com as op��es:
	* sincronizar os atestados incluidos com o sistema web | esse item seria o de compartilhar
	* Configur��o sem a��o
	* logout que volta para tela de login
* Tela de listar atestados com:
	* bot�o de adicionar que vai para outra act adicionar atestado
	* bot�o de buscar atestados cadastrados no sistema web
* Tela de adicionar atestados com:
	* bot�o de voltar para a lista de atestado
	
### 3� entrega ###
* ListView implementada de atestados
* Tela de inser��o de Atestados
* Tela de atualiza��o de atestado, mesma de cima com fun��o diferente.
* Busca na toolbar por um atestado, como filtro